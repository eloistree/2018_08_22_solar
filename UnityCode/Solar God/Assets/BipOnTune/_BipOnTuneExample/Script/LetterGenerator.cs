﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LetterGenerator : MonoBehaviour {

    public Text m_lastletterDisplay;
    public Text m_text;

	// Use this for initialization
	public void GenerateLetter (string letter) {

        Debug.Log("Create letter:" + letter);
        m_lastletterDisplay.text = letter;
        m_text.text += letter;

    }
	
}
