﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;

public class BipOnTune : MonoBehaviour {

    public bool removeToDO=true;
    public TrackActions m_track;

    public void Remove(int letterCount) {
        m_track.m_keyframes.RemoveRange(m_track.m_keyframes.Count - letterCount, letterCount);
    }
    
     string m_jsonExporter;
     string m_jsonImporter;


	public void AddKeyFrame ( float time, TuneKeyFrame keyframe) {

        TuneKeyFrame copy =AddKeyFrame(keyframe);
        copy.m_time = time;
    }


    public TuneKeyFrame AddKeyFrame(TuneKeyFrame keyframe)
    {
        TuneKeyFrame copy = keyframe.GetCopy();
        m_track.Add(copy);
        return copy;
    }

    internal List<TuneKeyFrame> GetActionsToDo(float start, float end)
    {
        return m_track.m_keyframes.Where(k => k.m_time > start && k.m_time < end).ToList();
    }
    internal List<string> GetActionsDescriptiono(float start, float end)
    {
        return m_track.m_keyframes.Where(k => k.m_time > start && k.m_time < end).Select(k=>k.m_descriptionName).ToList();
    }

    private void OnValidate()
    {
     

        directoryPath = Application.persistentDataPath + "/BipOnTune/";
        filePath = Application.persistentDataPath + "/BipOnTune/" + m_track.m_name + ".json";

        if (!string.IsNullOrEmpty(m_jsonImporter))
        {
            m_track = JsonUtility.FromJson<TrackActions>(m_jsonImporter);
            m_jsonImporter = "";

        }

        if (File.Exists(filePath)) {
            Import();
            DestroyFile(filePath);
        }
    }

    public void Clear()
    {
        m_track.Clear();
    }

    private void DestroyFile(string filePath)
    {
        File.Delete(filePath);
    }

    private void OnDestroy()
    {
        Export();
    }

    public string directoryPath;
    public string filePath;
    public void Export()
    {
        m_jsonExporter = JsonUtility.ToJson(m_track);

        Directory.CreateDirectory(directoryPath);
        File.WriteAllText(filePath, m_jsonExporter);
        m_jsonExporter = "";

    }
    public void Import()
    {
        m_jsonImporter = File.ReadAllText(filePath);
        m_track = JsonUtility.FromJson<TrackActions>(m_jsonImporter);
        m_jsonImporter = "";


    }
    
}
