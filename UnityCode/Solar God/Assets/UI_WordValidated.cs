﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_WordValidated : MonoBehaviour {

    public Text m_text;
    public Animator m_animator;

    public void SetWord(string word) {
        m_text.text = word;
        m_animator.SetTrigger("SendWord");
    }
	public void GoToNext () {
        m_animator.SetTrigger("GoToNext");

    }
    
}
