﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetClientsAsTransmittor : MonoBehaviour {

    IEnumerator Start()
    {
        while (!NetworkAutoLoad.IsServer())
        {
            KeystrokeUtility.Network.SwitchTransmissionTo(TransmissionType.Transmitter);
            yield return new WaitForSeconds(1f);

        }

        yield break;
    }


}
