﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_WordsValidated : MonoBehaviour {


    public UI_WordValidated[] m_words;
    public UI_WordValidated m_previous;

    public List<string> m_wordList;
    public float m_minDelay=0.1f;
    public float m_maxTimeDisplay = 1;

    public int m_index;
    public void AddWord(string word) {
        if(m_previous!=null)
        m_previous.GoToNext();
        m_previous = m_words[m_index];
        m_previous.SetWord(word);
        m_index++;
        if (m_index >= m_words.Length)
            m_index = 0;
        m_maxTimeDisplayCount = m_maxTimeDisplay;
    }

    private float m_maxTimeDisplayCount;
    public void Update()
    {
        if (m_maxTimeDisplayCount > 0f)
        {

            m_maxTimeDisplayCount -= Time.deltaTime;
            if (m_maxTimeDisplayCount<0f) {
                m_maxTimeDisplayCount = 0f;
                if (m_previous != null)
                    m_previous.GoToNext();
            }
        }
    }
}
