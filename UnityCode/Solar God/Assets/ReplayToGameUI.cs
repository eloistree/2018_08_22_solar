﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReplayToGameUI : MonoBehaviour {

    public Text m_text;
	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    public void GenerateTuneKeyFrame(TuneKeyFrame keyframe) {

        if(m_text!=null)
        m_text.text = keyframe.m_descriptionName;

    }
}
