﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using WindowsInput;

public class HostClientSceneLoader : MonoBehaviour {

    public string m_serverScene;
    
    public string m_clientSceneAndroid;
    public string m_clientSceneStandalone;
    public string m_clientSceneEditor;
    public ForcePlatform m_forcePlatform;
    public enum ForcePlatform { None,
        Android, Standalone
    }
    InputSimulator m_winSim;
    void Awake () {
        m_winSim = new InputSimulator();



        if (NetworkAutoLoad.IsServer())
        {
            SceneManager.LoadScene(m_serverScene, LoadSceneMode.Additive);
        }
        else {
            if (Application.platform ==RuntimePlatform.WindowsPlayer &&  m_winSim.InputDeviceState.IsKeyDown(WindowsInput.Native.VirtualKeyCode.SPACE))
            {
                SceneManager.LoadScene(m_clientSceneEditor, LoadSceneMode.Additive);
            }
            else if (Application.platform == RuntimePlatform.Android || m_forcePlatform == ForcePlatform.Android)
            {
                SceneManager.LoadScene(m_clientSceneAndroid, LoadSceneMode.Additive);
            }
            else {
                SceneManager.LoadScene(m_clientSceneStandalone, LoadSceneMode.Additive);
            }
        }
        Destroy(this);
	}
	
}
