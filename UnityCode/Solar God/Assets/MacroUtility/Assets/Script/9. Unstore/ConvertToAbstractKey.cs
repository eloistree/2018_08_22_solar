﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ConvertToAbstractKey : MonoBehaviour {

    public KeyBindingTable.OnKeyEvent m_onKeyDown;
    public KeyBindingTable.OnKeyEvent m_onKeyUp;


    public void NotifyKeyDown(KeyboardTouch key) { if(m_onKeyDown!=null) m_onKeyDown.Invoke(key); }
    public void NotifyKeyUp(KeyboardTouch key) { if (m_onKeyUp != null) m_onKeyUp.Invoke(key); }

}
