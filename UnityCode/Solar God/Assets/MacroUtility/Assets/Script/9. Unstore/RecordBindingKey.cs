﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using WindowsInput.Native;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class RecordBindingKey : MonoBehaviour {

    public string m_macroName;
    public string m_macroFileName;
    public Macro m_macroToReplay=  new Macro();

    public UnityEvent m_onChanged;
     
    public void AddKeyDown(KeyboardTouch key)
    {
        m_macroToReplay.AddKeyCodeDown(key);
        m_onChanged.Invoke();
    }
    public void AddKeyUp(KeyboardTouch key)
    {
        m_macroToReplay.AddKeyCodeUp(key);
        m_onChanged.Invoke();
    }

    public void AddCharacterStroke(char charater)
    {
        m_macroToReplay.AddCharacterStroke(charater);
        m_onChanged.Invoke();

    }
    public void AddCharactersStroke(string charaters)
    {
        for (int i = 0; i < charaters.Length; i++)
        {
            AddCharacterStroke(charaters[i]);   
        }
        m_onChanged.Invoke();

    }
    public void AddTextStroke(string text) {

        m_macroToReplay.AddTextStroke(text);
        m_onChanged.Invoke();

    }

    public void Start()
    {
        Clear();
    }
    public void Clear() {
        m_macroToReplay = new Macro();
        m_macroToReplay.AddWait(m_wait);
        m_onChanged.Invoke();
    }
    public float m_wait=3000;

    public AbstractKeyEvents events = new AbstractKeyEvents();

    public void ExecutreMacro() {
        events.m_onDownEvent += DebugDown;
        events.m_onUpEvent += DebugUp;
        StartCoroutine( MacroPlayer.GetPlayCoroutine(m_macroToReplay, MacroPlayer.PlayType.All, events));
    }

    private void DebugDown(KeyboardTouch keyCode)
    {
        Debug.Log("Down " + keyCode);

    }
    private void DebugUp(KeyboardTouch keyCode)
    {
        Debug.Log("Up " + keyCode);

    }

    public void SaveMacro() {
        string macroSave = MacroCompressor.GetCompressionOf(m_macroToReplay);
        Debug.Log("Saving...\n" + macroSave);
        m_macroToReplay.m_macroName = m_macroName;
        MacroStorage.Save(m_macroFileName, m_macroToReplay);
#if UNITY_EDITOR
        AssetDatabase.Refresh();
#endif

    }

    

    
}
