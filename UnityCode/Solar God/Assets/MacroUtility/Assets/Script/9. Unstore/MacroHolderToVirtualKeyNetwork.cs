﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WindowsInput.Native;

public class MacroHolderToVirtualKeyNetwork : MonoBehaviour {

    public VirtualKeyToNetwork m_network;
    public MacroHolder m_macroHolder;

    public void PlayEvents()
    {
        Play(MacroPlayer.PlayType.Events);
    }

    public void PlayAll()
    {
        Play(MacroPlayer.PlayType.All);
    }
    public void Play(MacroPlayer.PlayType playType)
    {
        AbstractKeyEvents callback = new AbstractKeyEvents();

        callback.m_onDownEvent = DownForward;
        callback.m_onUpEvent = UpForward;
        Coroutine cor = StartCoroutine(MacroPlayer.GetPlayCoroutine(m_macroHolder.m_macro, playType, callback));
    }

    private void UpForward(KeyboardTouch keyCode)
    {
        m_network.KeyUp(keyCode);
    }

    private void DownForward(KeyboardTouch keyCode)
    {
        m_network.KeyDown(keyCode);
    }
}
