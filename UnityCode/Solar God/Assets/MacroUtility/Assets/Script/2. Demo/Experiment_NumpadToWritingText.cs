﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Experiment_NumpadToWritingText : MonoBehaviour {

    public KeyboardKeyListener m_keyboardListener;

    public bool m_listenToMacro;
    public float m_lastStrokeTime;

    public List<KeyboardTouch> m_lastStroke = new List<KeyboardTouch>();

    public DigitalToText[] m_digiMacros = new DigitalToText[1];

    [System.Serializable]
    public class DigitalToText {
        public string m_text = "Hello Macro.";
        public int[] m_digits= new int[] {6,5,4 };

        public bool IsMacro(int[] digits) {
            if (m_digits.Length != digits.Length)
                return false;

            for (int i = 0; i < digits.Length; i++)
            {
                if (digits[i] != m_digits[i])
                    return false;
            }

            return true;
        }
        public string GetText() { return m_text; }
    }

	// Use this for initialization
	void Start ()
    {
        m_keyboardListener.m_onKeyDown.AddListener(KeyDown);
        m_keyboardListener.m_onKeyUp.AddListener(KeyUp);
    }

    private void KeyUp(KeyboardTouch touch)
    {
        if (touch == KeyboardTouch.LeftControl)
            m_listenToMacro = false;

    }
    private void KeyDown(KeyboardTouch touch)
    {
     

        if (touch == KeyboardTouch.LeftControl)
            m_listenToMacro = true;
        if (IsTouchDigitalNumpad(touch))
        {
            m_lastStrokeTime = 0;
            m_lastStroke.Add(touch);
        }
    }

    private bool IsTouchDigitalNumpad(KeyboardTouch touch)
    {
        bool digital = false;

        switch (touch)
        {
            
            case KeyboardTouch.NumPad0:
            case KeyboardTouch.NumPad1:
            case KeyboardTouch.NumPad2:
            case KeyboardTouch.NumPad3:
            case KeyboardTouch.NumPad4:
            case KeyboardTouch.NumPad5:
            case KeyboardTouch.NumPad6:
            case KeyboardTouch.NumPad7:
            case KeyboardTouch.NumPad8:
            case KeyboardTouch.NumPad9:
                digital = true;
                break;
            default:
                break;
        }

        return digital;
    }


    // Update is called once per frame
    void Update ()
    {
        if (m_listenToMacro)
        {
            float previousTime = m_lastStrokeTime;
            float currentTime = m_lastStrokeTime + Time.deltaTime; ;

            if ((previousTime < 1f && currentTime >= 1f && m_lastStroke.Count > 0) || (m_lastStroke.Count >= 3))
            {
                //  Debug.Log("Confirmed macro: ");
                //Validate Last macro
                int[] key = GetIntFromNumpad(m_lastStroke);
                for (int i = 0; i < m_digiMacros.Length; i++)
                {
                    if (m_digiMacros[i].IsMacro(key))
                    {

                        Debug.Log("Play: " + m_digiMacros[i].GetText());
                        KeystrokeUtility.StrokeText(m_digiMacros[i].GetText());
                        KeystrokeUtility.StrokeCharacter('\n');
                    }
                }

                m_lastStrokeTime = 0f;
                m_lastStroke.Clear();
            }


            m_lastStrokeTime = currentTime;
        }
        else {


            m_lastStrokeTime = 0f;
            m_lastStroke.Clear();
        }



    }

    private int[] GetIntFromNumpad(List<KeyboardTouch> m_lastStroke)
    {
        int[] nums = new int[m_lastStroke.Count];
        for (int i = 0; i < m_lastStroke.Count; i++)
        {
            nums[i] = ConvertTouchToInt(m_lastStroke[i]);
        }
        return nums;
    }

    private int ConvertTouchToInt(KeyboardTouch touch)
    {

        switch (touch)
        {
            
            case KeyboardTouch.NumPad1: return 1;
            case KeyboardTouch.NumPad2: return 2;
            case KeyboardTouch.NumPad3: return 3;
            case KeyboardTouch.NumPad4: return 4;
            case KeyboardTouch.NumPad5: return 5;
            case KeyboardTouch.NumPad6: return 6;
            case KeyboardTouch.NumPad7: return 7;
            case KeyboardTouch.NumPad8: return 8;
            case KeyboardTouch.NumPad9: return 9;
            default:
                break;
        }
        return 0;
    }

    public void OnValidate() {
        if (m_keyboardListener == null)
            m_keyboardListener = FindObjectOfType<KeyboardKeyListener>();
    }
}
