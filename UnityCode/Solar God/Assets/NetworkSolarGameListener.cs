﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkSolarGameListener : MonoBehaviour
{

    [Header("Debug")]
    public Text m_touchReceivedDebug;

    // Use this for initialization
    void Start()
    {
        KeystrokeUtility.InvokeDefaultListener();
        KeystrokeUtility.Network.onKeyDown += PressKeyDown;
        KeystrokeUtility.Network.onKeyUp += PressKeyUp;
        KeystrokeUtility.Network.onStrokeRequestCharacter += StrokeChar;
        KeystrokeUtility.Network.onStrokeRequestText += StrokeText;
    }
    private void OnDestroy()
    {

        KeystrokeUtility.Network.onKeyDown -= PressKeyDown;
        KeystrokeUtility.Network.onKeyUp -= PressKeyUp;
        KeystrokeUtility.Network.onStrokeRequestCharacter -= StrokeChar;
        KeystrokeUtility.Network.onStrokeRequestText -= StrokeText;
    }

    private void StrokeText(NetworkTransmissionInfo info, TextStrokeRequest text)
    {
        if(text.m_text.Length>0)
            CharTyped(info, text.m_text[0]);
    }

    private void StrokeChar(NetworkTransmissionInfo info, CharacterStrokeRequest character)
    {
        CharTyped(info, character.m_character);
    }

    private void PressKeyUp(NetworkTransmissionInfo info, KeyboardTouchPressRequest touch)
    {
        CharTyped(info, '-');

    }

    private void PressKeyDown(NetworkTransmissionInfo info, KeyboardTouchPressRequest touch)
    {
        CharTyped(info, '_');
        //bool isConvertable;
        //char key;
        //KeyBindingTable.ConvertTouchToChar(touch.m_touch, out key, out isConvertable)

    }


    private void CharTyped(NetworkTransmissionInfo info, char character) {

        m_touchReceivedDebug.text="Char: "+character+ "  ("+info.m_sender+")";

    }


}


