﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILyrics : MonoBehaviour {

    public int m_maxCharacter = 30;
    public Text [] m_lyrics;



    private void Update()
    {
      //  SetText(GetText());
    }

    public string GetText() {
        string text = "";
        for (int i = 0; i < m_lyrics.Length; i++)
        {
            text = m_lyrics[i].text + text;

        }
        return text;
    }
    public void SetText(string text ) {

        Debug.Log("Text"+ text);
        for (int i = 0; i < m_lyrics.Length; i++)
        {
            if (text.Length >= m_maxCharacter)
            {
                m_lyrics[i].text = text.Substring(text.Length - m_maxCharacter);
                text = text.Substring(0, text.Length - m_maxCharacter);
            }

            else if (text.Length > 0) {
                m_lyrics[i].text =text;
                text = "";
            }
            else m_lyrics[i].text = "";
        }


    }

    //public void Update()
    //{
    //    AddLeter((char)Random.RandomRange(0, 255));
    //}

    public void Add(TuneKeyFrame frame) {
        if (frame != null && frame.m_descriptionName.Length == 1) 
         AddLeter( frame.m_descriptionName[0]);
    }

    void AddLeter (char character) {
        m_lyrics[0].text += character;
        for (int i = 0; i < m_lyrics.Length; i++)
        {

            if (m_lyrics[i].text.Length > m_maxCharacter) {
                string sr = m_lyrics[i].text.Substring(0, m_lyrics[i].text.Length - m_maxCharacter);
                m_lyrics[i].text = m_lyrics[i].text.Substring(m_lyrics[i].text.Length - m_maxCharacter, m_maxCharacter);
                if (i + 1 >= 0 && i + 1 < m_lyrics.Length)
                    m_lyrics[i + 1].text += sr;
            }
        }
		
	}
}
