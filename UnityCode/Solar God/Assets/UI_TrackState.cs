﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_TrackState : MonoBehaviour {

    public KillTheLyrics m_killScore;
    public UserState m_eminem;
    public UserState m_user;

    public GetTimeFrom m_time;
    public bool hasBeenPlayed;

    [System.Serializable]
    public class UserState {
        public void SetPourcent(float pct) {
            m_slide.fillAmount = pct;
            m_userRect.anchorMin = new Vector2(0, pct);
            m_userRect.anchorMax = new Vector2(1, pct);
        }
        public float GetPourcent() { return m_slide.fillAmount; }
        public Image m_slide;
        public RectTransform m_userRect;
    }

	
	void Update () {
        if( m_user.GetPourcent()>0f && m_time.GetTime()<=0f)
            m_eminem.SetPourcent(1);
        else
            m_eminem.SetPourcent(m_time.GetPourcent());
        m_user.SetPourcent(m_killScore.GetPourcent());

    }
}
