﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeystrokeHistoryDebug : MonoBehaviour {
    
    public List<string> onStrokeReceivedHistory;
    public List<string> onNetworkStrokeReceivedHistory;
    public int listSize=10;

    // Use this for initialization
    void Start ()
    {
        KeystrokeUtility.onKeyDown += KeyDown;
        KeystrokeUtility.onKeyUp += KeyUp;
        KeystrokeUtility.onStrokeRequestText += AskText;
        KeystrokeUtility.Network.onStrokeRequestText += NetworkText;
        KeystrokeUtility.Network.onStrokeRequestCharacter += NetworkCharacter;
        KeystrokeUtility.Network.onKeyDown += NetworkKeyDown;
        KeystrokeUtility.Network.onKeyUp += NetworkKeyUp;
    }
    private void AddToQueue(ref List<string> list, string text)
    {
        if (list.Count >= listSize)
            list.RemoveAt(0);
        list.Add(text);
    }

    private void NetworkKeyDown(NetworkTransmissionInfo fromTo, KeyboardTouchPressRequest touch)
    {
        AddToQueue(ref onNetworkStrokeReceivedHistory, string.Format("From Net -Press-> {0}({1})", touch.m_touch, touch.m_pression));
    }


    private void NetworkKeyUp(NetworkTransmissionInfo fromTo, KeyboardTouchPressRequest touch)
    {
        AddToQueue(ref onNetworkStrokeReceivedHistory, string.Format("From Net-Press-> {0}({1})", touch.m_touch, touch.m_pression));
    }

    private void NetworkCharacter(NetworkTransmissionInfo fromTo, CharacterStrokeRequest character)
    {
        AddToQueue(ref onNetworkStrokeReceivedHistory, string.Format("From Net-Stroke-> {0}", character.m_character));
    }

    private void NetworkText(NetworkTransmissionInfo fromTo, TextStrokeRequest text)
    {
        AddToQueue(ref onNetworkStrokeReceivedHistory, string.Format("From Net-Stroke-> {0}", text.m_text));
    }
    

    private void AskText( TextStrokeRequest text)
    {
        AddToQueue(ref onStrokeReceivedHistory, string.Format("Native -Stroke-> {0}", text.m_text));
    }

    private void KeyUp( KeyboardTouchPressRequest touch)
    {
        AddToQueue(ref onStrokeReceivedHistory, string.Format("Native -Press-> {0}({1})", touch.m_touch, touch.m_pression));
    }

    private void KeyDown( KeyboardTouchPressRequest touch)
    {
        AddToQueue(ref onStrokeReceivedHistory, string.Format("Native -Press-> {0}({1})", touch.m_touch, touch.m_pression));
    }

    private void OnDestroy()
    {
        KeystrokeUtility.onKeyDown -= KeyDown;
        KeystrokeUtility.onKeyUp -= KeyUp;
        KeystrokeUtility.onStrokeRequestText -= AskText;
        KeystrokeUtility.Network.onStrokeRequestText -= NetworkText;
        KeystrokeUtility.Network.onStrokeRequestCharacter -= NetworkCharacter;
        KeystrokeUtility.Network.onKeyDown -= NetworkKeyDown;
        KeystrokeUtility.Network.onKeyUp -= NetworkKeyUp;
    }
    
}
