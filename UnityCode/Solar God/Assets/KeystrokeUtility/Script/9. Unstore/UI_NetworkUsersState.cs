﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_NetworkUsersState : MonoBehaviour {

    public GameObject m_userUiPrefab;
    public Transform m_rootPanel;
	// Use this for initialization
	void Refresh () {
        RemoveChild(m_rootPanel);
        string[] usersName = KeystrokeUtility.Network.GetUsersId();
        for (int i = 0; i < usersName.Length; i++)
        {
            KeyboardState keyboard = KeystrokeUtility.Network.GetUserPlatformKeyboardState(usersName[i]);
            GameObject pref = GameObject.Instantiate(m_userUiPrefab);
            UI_NetworkUserState user = pref.GetComponent<UI_NetworkUserState>();
            pref.SetActive(true);
            user.m_keyboardState.SetKeyboard(keyboard);
            user.m_userName.text = usersName[i];
        }
    }

    private void RemoveChild(Transform root)
    {
        int childs = root.childCount;
        for (int i = childs - 1; i > 0; i--)
        {
            GameObject.Destroy(transform.GetChild(i).gameObject);
        }
    }

    IEnumerator Start () {

        while (true) {
            yield return new WaitForSeconds(1);
            Refresh();
        }
	}
}
