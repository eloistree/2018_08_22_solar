﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonTouchesSequence : MonoBehaviour
{
    [System.Serializable]
    public class MechanicalActionTouch {
        public KeyboardTouch m_touch;
        public MechnicalActionType m_action;
    }

    public MechanicalActionTouch [] m_touches;
    public float m_timeBetween = 0.1f;

    public void SendTouchesToOtherDevices()
    {

        StartCoroutine(StartStroking());
    }

    private IEnumerator StartStroking()
    {
        for (int i = 0; i < m_touches.Length; i++)
        {
            SendTouchToOtherDevices(m_touches[i]);
            yield return new WaitForSeconds(m_timeBetween);

        }
    }

    public void SendTouchToOtherDevices(MechanicalActionTouch touch)
    {
        if (touch.m_action == MechnicalActionType.Down || touch.m_action == MechnicalActionType.Pressed)
            KeystrokeUtility.Network.PressDownTouch(touch.m_touch);
        if (touch.m_action == MechnicalActionType.Up || touch.m_action == MechnicalActionType.Pressed)
            KeystrokeUtility.Network.PressUpTouch(touch.m_touch);
    }
}
