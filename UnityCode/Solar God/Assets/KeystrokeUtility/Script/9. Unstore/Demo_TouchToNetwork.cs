﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WindowsInput.Native;

public class Demo_TouchToNetwork : MonoBehaviour
{

    public bool m_isOn;
    public void SetListenToTouch(bool value)
    {
        m_isOn = value;
    }
    void Awake()
    {
        KeystrokeUtility.onNativeWindowKeyCode += NetworkPressKey;

    }


    void OnDestroy()
    {
        KeystrokeUtility.onNativeWindowKeyCode -= NetworkPressKey;

    }

    private void NetworkPressKey(VirtualKeyCode value, bool isDown)
    {

        if (m_isOn)
        {
            if (KeystrokeUtility.Network.IsTransmitter())
            {

                KeyboardTouch touch;
                bool isConvertable;
                KeyBindingTable.ConvertWindowVirtualKeyCodesToTouch(value, out touch, out isConvertable);
                if (isConvertable)
                {
                    Debug.Log("> Win " + value + " to " + touch + " | " + isDown);
                    if (isDown)
                        KeystrokeUtility.Network.PressDownTouch(touch);
                    else
                        KeystrokeUtility.Network.PressUpTouch(touch);

                }
                else Debug.Log("Can't convert touch " + touch);
            }
        }
    }





}
