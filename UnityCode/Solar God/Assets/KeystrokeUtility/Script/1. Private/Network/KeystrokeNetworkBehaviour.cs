﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using WindowsInput.Native;
public class KeystrokeNetworkBehaviour : NetworkBehaviour
{
    //public IEnumerator Start()
    //{
    //    while (true)
    //    {
    //        yield return new WaitForSeconds(1);

    //        string[] names = NetworkUsersName.GetOthers();
    //        Debug.Log("Other: " + GetStringOf(names));
    //        names = NetworkUsersName.GetUserNames().ToArray();
    //        Debug.Log("All: " + GetStringOf(names));
    //        Debug.Log("Mine: " + NetworkUsersName.GetMine());
    //    }

    //}

    public void Awake()
    {
        KeystrokeUtility.Network.onTransmissionChanged += SwitchOnTransmissionChange;
        KeystrokeUtility.Network.SwitchTransmissionTo(m_transmitionType);

    }
    public void OnDestroy()
    {
        KeystrokeUtility.Network.onTransmissionChanged -= SwitchOnTransmissionChange;

    }
    //void OnPlayerConnected(NetworkPlayer player)
    //{
    //    Debug.Log("Player connected from " + player.ipAddress + ":" + player.port);
    //    SwitchTransmissionTo(m_transmitionType);
    //}

    void SwitchOnTransmissionChange(TransmissionType type)
    { SwitchTransmissionTo(type); }

    //[Command]
    //private void CmdStrokeChar(char c)
    //{
    //    RpcStokeChar(c);
    //}
    //[ClientRpc]
    //private void RpcStokeChar(char c)
    //{
    //    KeystrokeUtility.Network.onStrokeRequestCharacter(new CharacterStrokeRequest(c));
    //}

    //[Command]
    //private void CmdStrokeText(string t)
    //{
    //    RpcStrokeText(t);
    //}
    //[ClientRpc]
    //private void RpcStrokeText(string t)
    //{
    //    KeystrokeUtility.Network.onStrokeRequestText(new TextStrokeRequest(t));
    //}

     #region TEXT TO NETWORK
    public void StrokeCharacter(char c)
    {
        string[] names = NetworkUsersName.GetOthers();
        StrokeCharacter(c, names);

    }
    public void StrokeCharacter(char c,  string[] targetUsers)
    {
        if (!IsTransmitter())
            return;
        if (isLocalPlayer && hasAuthority)
            CmdStrokeChar(c,NetworkUsersName.GetMine(), targetUsers);
    }
    [Command]
    private void CmdStrokeChar(char c,string sender, string[] targetUsers)
    {
        RpcStrokeChar(c,sender, targetUsers);
    }
    [ClientRpc]
    private void RpcStrokeChar(char c,string sender, string[] targetUsers)
    {
        if (!IsReceptor())
            return;
        string name = NetworkUsersName.GetMine();
        bool isAllow = IsContainMyName(name, targetUsers);
        //if(isAllow)
        //    Debug.Log("Allow ? " + isAllow + "    " + name + "  l: " + GetStringOf(targetUsers));
        if (!isAllow) return;   

        if (KeystrokeUtility.Network.onStrokeRequestCharacter != null)
            KeystrokeUtility.Network.onStrokeRequestCharacter(new NetworkTransmissionInfo(sender, targetUsers), new CharacterStrokeRequest(c));

    }
    #endregion


    #region TEXT TO NETWORK
    public void StrokeText(string text)
    {
        string[] names = NetworkUsersName.GetOthers();
        StrokeText(text, names);

    }
    public void StrokeText(string text, string[] targetUsers)
    {
        if (!IsTransmitter())
            return;
        if (isLocalPlayer && hasAuthority)
            CmdStrokeText(text,NetworkUsersName.GetMine(), targetUsers);
    }
    [Command]
    private void CmdStrokeText(string text,string sender, string[] targetUsers)
    {
        RpcStrokeText(text,sender, targetUsers);
    }
    [ClientRpc]
    private void RpcStrokeText(string text,string sender, string[] targetUsers)
    {
        if (!IsReceptor())
            return;
        string name = NetworkUsersName.GetMine();
        bool isAllow = IsContainMyName(name, targetUsers);
        //if(isAllow)
        //    Debug.Log("Allow ? " + isAllow + "    " + name + "  l: " + GetStringOf(targetUsers));
        if (!isAllow) return;
       
            if (KeystrokeUtility.Network.onStrokeRequestText != null)
                KeystrokeUtility.Network.onStrokeRequestText(new NetworkTransmissionInfo(sender, targetUsers), new TextStrokeRequest(text));
       
    }
    #endregion





    #region TOUCH TO NETWORK
    public void PressTouch(KeyboardTouch touch, bool isDown)
    {
        string[] names = NetworkUsersName.GetOthers();
        PressTouch(touch, isDown, names);

    }
    public void PressTouch(KeyboardTouch touch, bool isDown, string[] targetUsers)
    {
        if (!IsTransmitter())
            return;
        //        Debug.Log("Try to send:" + touch + "  l: " + GetStringOf(targetUsers));
        if (isLocalPlayer && hasAuthority)
            CmdPressTouch((int)touch, isDown,NetworkUsersName.GetMine(), targetUsers);
    }
    [Command]
    private void CmdPressTouch(int touchId, bool isDown,string sender, string[] targetUsers)
    {
        RpcPressTouch(touchId, isDown,sender, targetUsers);
    }
    [ClientRpc]
    private void RpcPressTouch(int touchId, bool isDown,string sender, string[] targetUsers)
    {
        if (!IsReceptor())
            return;
        string name = NetworkUsersName.GetMine();
        bool isAllow = IsContainMyName(name, targetUsers);
        //if(isAllow)
        //    Debug.Log("Allow ? " + isAllow + "    " + name + "  l: " + GetStringOf(targetUsers));
        if (!isAllow) return;
        KeyboardTouch touch = (KeyboardTouch)touchId;
        // Debug.Log("Succed:" + touch);
        if (isDown)
        {
            if (KeystrokeUtility.Network.onKeyDown != null)
                KeystrokeUtility.Network.onKeyDown(new NetworkTransmissionInfo(sender, targetUsers), new KeyboardTouchPressRequest(touch, PressType.Down));
        }
        else
        {

            if (KeystrokeUtility.Network.onKeyUp != null)
                KeystrokeUtility.Network.onKeyUp(new NetworkTransmissionInfo(sender, targetUsers), new KeyboardTouchPressRequest(touch, PressType.Up));
        }
    } 
    #endregion

    private string GetStringOf(string[] targetUsers)
    {
        string append = "";
        for (int i = 0; i < targetUsers.Length; i++)
        {
            append += targetUsers[i];
        }
        return append;
    }

    private bool IsContainMyName(string name, string[] targetUsers)
    {
        for (int i = 0; i < targetUsers.Length; i++)
        {
            if (name == targetUsers[i])
                return true;
        }
        return false;
    }
    internal  bool IsTransmitter()
    {
        return KeystrokeUtility.Network.IsTransmitter();
    }

    internal  bool IsReceptor()
    {
        return KeystrokeUtility.Network.IsReceptor();
    }


    public void SwitchTransmissionTo(TransmissionType transmission) {
        if (hasAuthority && isLocalPlayer)
            CmdSwitchTransmissionTo(transmission.ToString());
    }
    [Command]
    private void CmdSwitchTransmissionTo(string transmission)
    {
        m_transmitionType = (TransmissionType)Enum.Parse(typeof(TransmissionType), transmission);


    }
    [SyncVar]
    public  TransmissionType m_transmitionType = TransmissionType.Receptor;
}
