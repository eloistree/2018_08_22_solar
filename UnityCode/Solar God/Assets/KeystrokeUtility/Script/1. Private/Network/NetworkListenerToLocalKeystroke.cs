﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkListenerToLocalKeystroke : MonoBehaviour {

	// Use this for initialization
	void Start () {

        KeystrokeUtility.Network.onKeyDown += PressKeyDown;
        KeystrokeUtility.Network.onKeyUp += PressKeyUp;
        KeystrokeUtility.Network.onStrokeRequestCharacter += StrokeChar;
        KeystrokeUtility.Network.onStrokeRequestText += StrokeText;
    }
    private void OnDestroy()
    {

        KeystrokeUtility.Network.onKeyDown -= PressKeyDown;
        KeystrokeUtility.Network.onKeyUp -= PressKeyUp;
        KeystrokeUtility.Network.onStrokeRequestCharacter -= StrokeChar;
        KeystrokeUtility.Network.onStrokeRequestText -= StrokeText;
    }

    private void StrokeText(NetworkTransmissionInfo info, TextStrokeRequest text)
    {
        KeystrokeUtility.StrokeText(text.m_text);
    }

    private void StrokeChar(NetworkTransmissionInfo info, CharacterStrokeRequest character)
    {
        KeystrokeUtility.StrokeCharacter(character.m_character);
    }

    private void PressKeyUp(NetworkTransmissionInfo info, KeyboardTouchPressRequest touch)
    {

        KeystrokeUtility.PressKeyUp(touch.m_touch);
    }

    private void PressKeyDown(NetworkTransmissionInfo info, KeyboardTouchPressRequest touch)
    {
        KeystrokeUtility.PressKeyDown(touch.m_touch);
    }
    
}
