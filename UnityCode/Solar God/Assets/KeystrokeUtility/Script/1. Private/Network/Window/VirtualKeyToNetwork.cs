﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using WindowsInput.Native;

public class VirtualKeyToNetwork : MonoBehaviour {

    public BroadcastVirtualKey m_instance;

    public void KeyDown(KeyboardTouch key)
    {
        if (CheckForInstanceInScene()) {
            
            m_instance.PushVirtualKeyDownToOthers(key);
        }
    }

   

    public void KeyUp(KeyboardTouch key)
    {
        if (CheckForInstanceInScene())
        {
            m_instance.PushVirtualKeyUpToOthers(key);
        }
    }


    private bool CheckForInstanceInScene()
    {
        if (m_instance == null)
        {
            BroadcastVirtualKey[] list = FindObjectsOfType<BroadcastVirtualKey>().Where(t => t.hasAuthority && t.isLocalPlayer).ToArray();
            if (list.Length > 0)
                m_instance = list[0];
        }
        return m_instance != null;
    }

    public void KeyAction(KeyboardTouch key, MechnicalActionType action)
    {
        if (action == MechnicalActionType.Down || action == MechnicalActionType.Pressed)
            KeyDown(key);
        if (action == MechnicalActionType.Up || action == MechnicalActionType.Pressed)
            KeyUp(key);

    }
}
