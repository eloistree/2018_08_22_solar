﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo_SendRandomNetworkStroke : MonoBehaviour {

    public float m_delay=0.5f;

    public bool m_ascii;
    public bool m_unicode;
    public bool m_text;
    public bool m_touch;
    public bool m_alpha;


    public void SetASCII(bool value) { m_ascii = value; }
    public void SetUnicode(bool value) { m_unicode = value; }
    public void SetText(bool value) { m_text = value; }
    public void SetTouch(bool value) { m_touch = value; }
    public void SetAlpha(bool value) { m_alpha = value; }

    public string [] m_someTexts =new string[] {
        "(-(-_(-_-)_-)-)",
        "┌∩┐(◣_◢)┌∩┐",
        "\\,,/(^_^)\\,,/",
        "(╯°□°）╯︵ ┻━┻",
        "~(‾▿‾)~",
        "༼☉ɷ⊙༽",
        " ‹^› ‹(•_•)› ‹^›",
        "'(◣_◢)'",
        " ლ(ಠ益ಠ)ლ",
        "(ಠ_ಠ)",
        "(\\/)(-,,-)(\\/)",
        "(. v .)"

    };


    private void OnEnable()
    {
        StartCoroutine(SendRandom());
    }
    IEnumerator SendRandom()
    {
        char c;
        string text;
        KeyboardTouch touch;
        KeyboardTouch[] alphas;
        bool capLockSwitch = false;

        alphas = KeyBindingTable.GetAlphaNumbers();

        while (true)
        {
            yield return new WaitForEndOfFrame();

            if (m_ascii) {
                yield return new WaitForSeconds(m_delay);
                Debug.Log("ASC.");
                c = KeyBindingTable.GetCharASCII(Random.Range(0, 256));
                KeystrokeUtility.Network.StrokeCharacter(c);
            }
            if (m_unicode)
            {
                yield return new WaitForSeconds(m_delay);
                Debug.Log("Uni.");
                c = KeyBindingTable.GetCharUnicode(Random.Range(100, 1000));
                KeystrokeUtility.Network.StrokeCharacter(c);
            }
            if (m_text)
            {

                yield return new WaitForSeconds(m_delay);
                Debug.Log("Text.");
                text = m_someTexts[Random.Range(0, m_someTexts.Length)];
                KeystrokeUtility.Network.StrokeText(text);

            }
            if (m_alpha)
            {
                yield return new WaitForSeconds(m_delay);
                Debug.Log("Alpha.");
                
                KeyboardTouch[] t = KeyBindingTable.GetAlphaCharacter();
                touch =t[Random.Range(0, t.Length)];
                KeystrokeUtility.Network.PressDownTouch(touch);
                yield return new WaitForSeconds(Random.Range(0.05f, 0.5f));
                KeystrokeUtility.Network.PressUpTouch(touch);
            }


        }
    }

}
