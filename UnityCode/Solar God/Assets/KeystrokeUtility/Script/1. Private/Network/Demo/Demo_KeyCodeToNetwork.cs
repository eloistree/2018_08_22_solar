﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo_KeyCodeToNetwork : MonoBehaviour {

    public bool m_isOn;
    public void SetListenToKeyboard(bool value)
    {
        m_isOn = value;
    }
    // Use this for initialization
    void Start()
    {
        KeystrokeUtility.onNativeUnityKeyCode += NetworkStrokeKeyCode;

    }

    void OnDestroy()
    {
        KeystrokeUtility.onNativeUnityKeyCode -= NetworkStrokeKeyCode;

    }
    

    private void NetworkStrokeKeyCode(KeyCode value, bool isDown)
    {
        if (m_isOn) { 
            KeyboardTouch touch = KeyboardTouch.Unkown;
            bool convertable;
            KeyBindingTable.ConvertUnityKeyToTouch(value, out touch, out convertable);

            if (convertable)
            {
                if (isDown)
                    KeystrokeUtility.Network.PressDownTouch(touch);
                else
                    KeystrokeUtility.Network.PressUpTouch(touch);
            }
            else Debug.Log("Can't convert " + value + " to Touch");
        }
    }
    
}
