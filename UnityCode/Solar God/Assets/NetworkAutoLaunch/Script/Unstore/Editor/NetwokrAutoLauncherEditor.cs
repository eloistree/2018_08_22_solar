﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

[CustomEditor(typeof(NetworkAutoLauncherMono))]
public class NetwokrAutoLauncherEditor : Editor
{
    public SceneAsset m_resourcesScene;
    public SceneAsset m_lanScene;
    public SceneAsset m_lobbyScene;
    public SceneAsset m_offlineScene;
    public SceneAsset m_onlineScene;

    public bool isFirstTime=true;
    
    public override void OnInspectorGUI()
    {
        NetworkAutoLauncherMono myTarget = (NetworkAutoLauncherMono)target;

        m_resourcesScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(myTarget.m_resourcesScenePath);

        m_lanScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(myTarget.m_lanScenePath);
        m_lobbyScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(myTarget.m_lobbyScenePath);

        m_offlineScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(myTarget.m_offlineScenePath);

        m_onlineScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(myTarget.m_onlineScenePath);


        //base.OnInspectorGUI();
        myTarget.m_userNameDefault = EditorGUILayout.TextField("User Name", myTarget.m_userNameDefault);
        myTarget.m_roomNameDefault = EditorGUILayout.TextField("Room Name", myTarget.m_roomNameDefault);
        myTarget.m_passwordDefault = EditorGUILayout.PasswordField("Password", myTarget.m_passwordDefault);
        myTarget.m_userPrefabDefault = (GameObject)EditorGUILayout.ObjectField(myTarget.m_userPrefabDefault, typeof(GameObject), true);

        GUILayout.Space(5);
        GUILayout.Label("Editor", EditorStyles.boldLabel);
        GUILayout.Space(3);
        LoadSceneAssetDisplay("Resources Scene:", ref myTarget.m_resourcesScenePath,ref myTarget.m_resourcesScene , m_resourcesScene);
        LoadSceneAssetDisplay("Lan Scene:", ref myTarget.m_lanScenePath, ref myTarget.m_lanScene, m_lanScene);
        LoadSceneAssetDisplay("Lobby Scene:", ref myTarget.m_lobbyScenePath, ref myTarget.m_lobbyScene, m_lobbyScene);
        LoadSceneAssetDisplay("Offline Scene:", ref myTarget.m_offlineScenePath, ref myTarget.m_offlineScene, m_offlineScene);
        LoadSceneAssetDisplay("Online Scene:", ref myTarget.m_onlineScenePath, ref myTarget.m_onlineScene, m_onlineScene);
        
        GUILayout.Space(8);

        if (GUILayout.Button("Add To Build Settings"))
        {
            SetEditorBuildSettingsScenes();
        }
    }
    //
    private void LoadSceneAssetDisplay(string label, ref string scenePath, ref string sceneName, SceneAsset sceneAsset )
    {
        GUILayout.Label(label, EditorStyles.boldLabel);
        sceneAsset = (SceneAsset)EditorGUILayout.ObjectField(sceneAsset, typeof(SceneAsset), false);
        if (sceneAsset != null) {
            string path = AssetDatabase.GetAssetPath(sceneAsset);
            if (!string.IsNullOrEmpty(path)) {
                scenePath = path;
                sceneName = sceneAsset.name;

            }
        }
    }

    public void SetEditorBuildSettingsScenes()
    {

            AddSceneToBuild(m_resourcesScene);
            AddSceneToBuild(m_lanScene);
            AddSceneToBuild(m_lobbyScene);
            AddSceneToBuild(m_offlineScene);
            AddSceneToBuild(m_onlineScene);
    }

    private static void AddSceneToBuild( SceneAsset sceneAsset)
    {

        string scenePath = AssetDatabase.GetAssetPath(sceneAsset);
        List<EditorBuildSettingsScene> editorBuildSettingsScenes = EditorBuildSettings.scenes.ToList();
        foreach (var i in editorBuildSettingsScenes)
        {
            Debug.Log("->-> "+i.path);
        }

        var d = editorBuildSettingsScenes.Where(k => k.path == scenePath);
        Debug.Log(d.Count()+"->" + scenePath);
        if (d.Count() <= 0)
        {
            if (!string.IsNullOrEmpty(scenePath)) {
                editorBuildSettingsScenes.Add(new EditorBuildSettingsScene(scenePath, true));
            }
            EditorBuildSettings.scenes = editorBuildSettingsScenes.ToArray();

        }
    }
}