﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class KillTheLyrics : MonoBehaviour {

    public int pointPrimaryByLetter=5;
    public int pointSecondByLetter = 1;
    public int pointByLetter=1;




    [Header("Script Ref")]
    public GetTimeFrom m_getTime;
    public UILyrics m_lyrics;
    public BipOnTuneReplay m_replay;
    public AudioSource m_source;

    [Header("UI")]
    public Text m_mainText;
    public Text m_secondText;
    public Text m_scoreDebug;
    public Text m_bestScore;

    [Header("Unity Event")]
    public OnWordFound m_onWordFound;

    [Header("Debug")]
    public float m_currentScore;
    public float m_uiScore;
    public int letterKilled;
        
        
    [Serializable]
    public class OnWordFound : UnityEvent<string> {

    }


    public void TryToKillLyrics(char character)
    {
        TryToKillLyrics(character, "Host");
    }

    public int m_typedCharactersByUsers;
    public int m_charTotal;

    internal float GetPourcent()
    {
        return (float) m_typedCharactersByUsers / (float) m_charTotal;
    }

    // Use this for initialization
    public   void TryToKillLyrics (char character, string userName) {

        string value = m_mainText.text;
        int indexOfCharacter = value.LastIndexOf(character);
        if (indexOfCharacter > 0) {
            char[] val = value.ToCharArray();
            val[indexOfCharacter] = ' ';
            m_mainText.text = new string(val);
            m_currentScore = pointPrimaryByLetter;
            letterKilled++;
        }


    }
    public void TryToKillLyrics(string word)
    {
        TryToKillLyrics(word, "Host");
    }


    public void TryToKillLyrics(string word, string userName)
    {
        string displayText = m_lyrics.GetText();


        if (!IsPlayingMusic())
            return;

        if (string.IsNullOrEmpty(word))
            return;

        string text = m_lyrics.GetText();

        if (string.IsNullOrEmpty(text))
            return;

        string result="";
        string textWithoutLastWord = GetTextWithoutLastProcessingWord(text);
        bool isWordInDisplayedText = ContaintWord(word, textWithoutLastWord);
        bool isTextStartWithBlank = GetFirstCharacter(text) == ' ';
        bool isTextEndWithBlank = GetLastCharacter(text) == ' ';

        string lastWord = GetLastWordOf(text);

        string primaryText = GetPrimaryTextWithouthInProcessWord(text);
        string secondaryText = m_secondText.text;

        AddScoreIfWordIn(word, m_mainText.text, pointPrimaryByLetter);
        AddScoreIfWordIn(word, m_secondText.text, pointSecondByLetter);
        AddScoreIfWordIn(word, text, pointByLetter);



        if (isWordInDisplayedText)
        {
            Debug.Log("Found: " + word);
            string empty = "";
            for (int i = 0; i < word.Length + 2; i++)
            {
                empty += " ";
            }

            m_typedCharactersByUsers += word.Length;
            textWithoutLastWord = textWithoutLastWord.Replace(" " + word + " ", " " + empty + " ");


            //while(text.IndexOf("  ")>=0)
            //    text = text.Replace("  ", " ");

            m_onWordFound.Invoke(word);


        }
        Debug.Log("Text Without:" + textWithoutLastWord);
        Debug.Log("Last word:" + lastWord);
        result = (isTextStartWithBlank ? " " : "") + (textWithoutLastWord.Trim() ) + (isTextEndWithBlank ? " " : " "+lastWord + "");

        m_lyrics.SetText(result);


    }

    private string GetTextWithoutLastProcessingWord(string text)
    {
        return GetLastCharacter(text) != ' ' ? GetTextWithoutLastWord(text) : text;
    }

    private string GetPrimaryTextWithouthInProcessWord(string text)
    {
        return GetLastCharacter(text) != ' ' ? GetTextWithoutLastWord(m_mainText.text) : m_mainText.text;
    }

    private void AddScoreIfWordIn(string word, string text, int pointPrimaryByLetter)
    {
        if (ContaintWord(word, text))
            m_currentScore += m_source.pitch * m_source.pitch * (pointPrimaryByLetter * word.Length);
    }

    private string GetTextWithoutLastWord(string text)
    {   string word;
        text=  RemoveLastWord(text, out word);
        return text;
    }

    private string GetLastWordOf(string text)
    {
        string word;
        RemoveLastWord(text, out word);
        return word;
    }

    private static char GetFirstCharacter(string text)
    {
        return text[0];
    }

    private static char GetLastCharacter(string text)
    {
        return text[text.Length - 1];
    }

    private bool IsPlayingMusic()
    {
        float timePct = m_getTime.GetPourcent();
        return timePct > 0f && timePct < 1f;
    }

    private string RemoveLastWord(string text, out string word)
    {
        word = "";

      List<char> letters = text.ToList<char>();
        while (letters.Count > 0 && letters[letters.Count - 1] != ' ') {
            word = letters[letters.Count - 1] + word;
            letters.RemoveAt(letters.Count - 1);
        }
        return new string(letters.ToArray());
    }

    private bool ContaintWord(string word, string text)
    {
        text = " "+text.Trim()+" ";
        return text.IndexOf(" " + word + " ") >= 0;
    }

    private void Start()
    {

        string[] frames = m_replay.GetFramesDescription(float.MaxValue);
        m_charTotal = frames.Where(k => !string.IsNullOrEmpty(k)).Count();
    }

    private void Update()
    {
        m_uiScore = Mathf.Lerp(m_uiScore, m_currentScore, Time.deltaTime);

        m_scoreDebug.text = "" + (int)m_uiScore;
        SetNewBestScore(m_uiScore);
        m_bestScore.text = "" + (int)GetBestScore();


        

    }

    private void SetNewBestScore(float score)
    {
        float best = PlayerPrefs.GetInt("BestScore");
        if(score > best)
            PlayerPrefs.SetInt("BestScore", (int)score);
    }

    public int GetBestScore() {
        return PlayerPrefs.GetInt("BestScore"); 
    }
}
